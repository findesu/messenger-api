import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import NoAccessException from 'App/Exceptions/NoAccessException'
import Channel from 'App/Models/Channel'
import { removeDuplicates, removeValues } from 'App/Utils/Array'
import ChannelMembersValidator from 'App/Validators/ChannelMembersValidator'

export default class ChannelMembersController
{
  public async index({ params, auth }: HttpContextContract)
  {
    const channel = await Channel.findOrFail(params.channel_id)
    const user = auth.user!

    if (!await channel.related('members').query().where('id', user.id).first())
      throw new NoAccessException('channel_member_index')
    
    await channel.load('members')

    return channel.members
  }

  public async store({ request, response, params, auth }: HttpContextContract)
  {
    const channel = await Channel.findOrFail(params.channel_id)
    const user = auth.user!

    if (!await channel.related('members').query().where('id', user.id).first())
      throw new NoAccessException('channel_members_store')
    
    const data = await request.validate(ChannelMembersValidator)

    await channel.load('members')
    
    data.ids = removeValues(data.ids, channel.members.map(user => user.id))

    await channel.related('members')
      .attach(removeDuplicates(data.ids))

    response.noContent()
  }

  public async destroy({ request, response, params, auth }: HttpContextContract)
  {
    const channel = await Channel.findOrFail(params.channel_id)
    const user = auth.user!

    if (!await channel.related('members').query().where('id', user.id).first())
      throw new NoAccessException('channel_members_delete')
    
    const data = await request.validate(ChannelMembersValidator)
    
    await channel.related('members')
      .detach(removeDuplicates(data.ids))
    
    if (!await channel.related('members').query().first())
      await channel.delete()
    
    response.noContent()
  }
}
