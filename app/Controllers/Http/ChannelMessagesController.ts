import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import NoAccessException from 'App/Exceptions/NoAccessException'
import Channel from 'App/Models/Channel'
import { pagination } from 'App/Utils/QS'
import ChannelMessageValidator from 'App/Validators/ChannelMessageValidator'

export default class ChannelMessagesController
{
  public async index({ request, params, auth }: HttpContextContract)
  {
    const channel = await Channel.findOrFail(params.channel_id)
    const user = auth.user!

    if (!await channel.related('members').query().where('id', user.id).first())
      throw new NoAccessException('channel_message_index')

    const query = channel.related('messages').query()
      .orderBy('createdAt', 'desc')

    const qs = request.qs()
    
    const { page, amount } = pagination(qs)

    return await query.paginate(page, amount)
  }

  public async store({ request, response, params, auth }: HttpContextContract)
  {
    const channel = await Channel.findOrFail(params.channel_id)
    const user = auth.user!

    if (!await channel.related('members').query().where('id', user.id).first())
      throw new NoAccessException('channel_message_store')
    
    const data = await request.validate(ChannelMessageValidator)
    const message = await channel.related('messages')
      .create({
        authorId: user.id,
        content: data.content,
      })
    
    response.created(message)
  }

  public async show({ params, auth }: HttpContextContract)
  {
    const channel = await Channel.findOrFail(params.channel_id)
    const user = auth.user!

    if (!await channel.related('members').query().where('id', user.id).first())
      throw new NoAccessException('channel_message_show')
    
    return await channel
      .related('messages')
      .query()
      .where('id', params.id)
      .firstOrFail()
  }

  public async update({ request, params, auth }: HttpContextContract)
  {
    const channel = await Channel.findOrFail(params.channel_id)
    const user = auth.user!

    if (!await channel.related('members').query().where('id', user.id).first())
      throw new NoAccessException('channel_message_edit')
    
    const message = await channel
      .related('messages')
      .query()
      .where('id', params.id)
      .firstOrFail()
    
    if (message.authorId !== user.id)
      throw new NoAccessException('channel_message_edit')

    const data = await request.validate(ChannelMessageValidator)

    return await message.merge(data).save()
  }

  public async destroy({ response, params, auth }: HttpContextContract)
  {
    const channel = await Channel.findOrFail(params.channel_id)
    const user = auth.user!

    if (!await channel.related('members').query().where('id', user.id).first())
      throw new NoAccessException('channel_message_edit')
    
    const message = await channel
      .related('messages')
      .query()
      .where('id', params.id)
      .firstOrFail()
    
    if (message.authorId !== user.id)
      throw new NoAccessException('channel_message_delete')
    
    await message.delete()

    response.noContent()
  }
}
