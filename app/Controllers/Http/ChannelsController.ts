import { Exception } from '@adonisjs/core/build/standalone'
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import NoAccessException from 'App/Exceptions/NoAccessException'
import Channel from 'App/Models/Channel'
import { removeDuplicates } from 'App/Utils/Array'
import ChannelCreateValidator from 'App/Validators/ChannelCreateValidator'

export default class ChannelsController
{
  public async index({ auth }: HttpContextContract)
  {
    const user = auth.user!

    return await Channel.query()
      .join('users_channels', query => 
      {
        query
          .on('channels.id', 'users_channels.channel_id')
          .onVal('users_channels.user_id', '=', user.id)
      })
  }

  public async store({ request, response, auth }: HttpContextContract)
  {
    const user = auth.user!
    const data = await request.validate(ChannelCreateValidator)
    
    data.members.push(user.id.toString())
    data.members = removeDuplicates(data.members)

    if (data.type === Channel.Type.DM && data.members.length !== 2)
      throw new Exception('You pass too much or too little members for this channel', 400, 'E_CHANNEL_MEMBERS_LIMIT')

    const channel = await Channel.create({
      type: data.type,
    })

    await channel.related('members')
      .attach(data.members)

    response.created(channel)
  }

  public async show({ params, auth }: HttpContextContract)
  {
    const channel = await Channel.findOrFail(params.id)
    const user = auth.user!

    if (!await channel.related('members').query().where('id', user.id).first())
      throw new NoAccessException('channel_show')
    
    return channel
  }

  public async destroy({ response, params, auth }: HttpContextContract)
  {
    const channel = await Channel.findOrFail(params.id)
    const user = auth.user!

    if (!await channel.related('members').query().where('id', user.id).first())
      throw new NoAccessException('channel_delete')

    await channel.delete()
    
    response.noContent()
  }
}
