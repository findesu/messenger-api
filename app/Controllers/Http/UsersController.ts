import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Drive from '@ioc:Adonis/Core/Drive'
import { cuid } from '@ioc:Adonis/Core/Helpers'
import { ModelAttributes } from '@ioc:Adonis/Lucid/Orm'
import User from 'App/Models/User'
import UserUpdateValidator from 'App/Validators/UserUpdateValidator'
import NoAccessException from 'App/Exceptions/NoAccessException'
import sharp from 'sharp'
import UploadException from 'App/Exceptions/UploadException'
import { pagination } from 'App/Utils/QS'

export default class UsersController
{
  public async index({ request }: HttpContextContract)
  {
    const query = User.query()
    const qs = request.qs()

    if (qs.search)
    {
      const search = `%${
        qs.search
          .toLowerCase()
          .replaceAll('%', '\\%')
          .replaceAll('_', '\\_')
      }%`

      query
        .where(query =>
          query
            .whereRaw('lower("name") like ?', [ search ])
            .orWhereRaw('lower("username") like ?', [ search ])
        )
    }

    const { page, amount } = pagination(qs)

    return await query.paginate(page, amount)
  }

  public async show({ params }: HttpContextContract)
  {
    return await User.findOrFail(params.id)
  }

  public async update({ request, params, auth }: HttpContextContract)
  {
    const user = await User.findOrFail(params.id)

    if (auth.user!.id !== user.id)
      throw new NoAccessException('user_edit')
    
    const data = await request.validate(UserUpdateValidator)
    const modified: Partial<ModelAttributes<User>> = { }
    const avatarFile = data.avatar

    if (avatarFile?.extname)
    {
      const metadata = await sharp(avatarFile.tmpPath).metadata()

      if (metadata.width! > 8192 || metadata.height! > 8192)
        throw UploadException.tooBigResolutionOfImage()

      const image = sharp(avatarFile.tmpPath)

      if (metadata.width! > 512 || metadata.height! > 512)
        image.resize(512, 512, { fit: 'cover' })
      
      image.webp()
      
      if (user.avatar)
        await Drive.delete(user.avatar)
      
      const avatar = `${cuid()}.webp`
      
      await Drive.put(avatar, await image.toBuffer())
      
      modified.avatar = avatar
    }

    modified.name = data.name

    if (data.confirmation)
    {
      await auth.verifyCredentials(user.username, data.confirmation)

      modified.username = data.username
      modified.email = data.email
      modified.password = data.password
    }

    return await user.merge(modified).save()
  }

  public async destroy({ response, params, auth }: HttpContextContract)
  {
    const user = await User.findOrFail(params.id)

    if (auth.user!.id !== user.id)
      throw new NoAccessException('user_delete')

    await user.delete()
    
    response.noContent()
  }
}
