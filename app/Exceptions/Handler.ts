import Logger from '@ioc:Adonis/Core/Logger'
import HttpExceptionHandler from '@ioc:Adonis/Core/HttpExceptionHandler'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { Exception } from '@adonisjs/core/build/standalone'
import { UnknownRecord } from 'App/Utils/Types'

type ErrorMessage = {
  message: string,
  code?: string,
} & UnknownRecord

export default class ExceptionHandler extends HttpExceptionHandler 
{
  constructor() 
  {
    super(Logger)
  }

  public async handle(exception: Exception & UnknownRecord, { response }: HttpContextContract) 
  {
    let error: ErrorMessage = {
      message: exception.message,
      code: exception.code,
    }
    
    if (exception.code === 'E_VALIDATION_FAILURE')
      error.rules = (exception.messages as UnknownRecord).errors
    else if ([ 'E_INVALID_AUTH_UID', 'E_INVALID_AUTH_PASSWORD' ].includes(exception.code ?? ''))
    {
      error = {
        message: 'Invalid credentials',
        code: 'E_INVALID_AUTH_CREDENTIALS',
      }
    }
    else if (typeof exception.advanced === 'function')
    {
      error = {
        ...exception.advanced(),
        ...error,
      }
    }

    response
      .status(exception.status)
      .send({ error })
  }
}
