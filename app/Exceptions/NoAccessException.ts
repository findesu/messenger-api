import { Exception } from '@adonisjs/core/build/standalone'

export default class NoAccessException extends Exception
{
  constructor(public permission: string)
  {
    super('You have not access', 403, 'E_NO_ACCESS')
  }

  public advanced()
  {
    return {
      permission: this.permission,
    }
  }
}
