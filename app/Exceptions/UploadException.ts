import { Exception } from '@adonisjs/core/build/standalone'

export default class UploadException extends Exception
{
  public static tooBigResolutionOfImage()
  {
    return new this('Too big resolution of image', 400, 'E_IMAGE_TOO_BIG_RESOLUTION')
  }
}
