import { DateTime } from 'luxon'
import { BaseModel, column, hasMany, HasMany, ManyToMany, manyToMany } from '@ioc:Adonis/Lucid/Orm'
import { field } from 'App/Utils/Validator'
import User from 'App/Models/User'
import Message from './Message'
import { values } from 'App/Utils/Enum'

enum ChannelType
{
  DM,
  GROUP,
}

export class Channel extends BaseModel 
{
  @column({ isPrimary: true })
  public id: number | string

  @column()
  public type: Channel.Type

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @hasMany(() => Message)
  public messages: HasMany<typeof Message>

  @manyToMany(() => User, { pivotTable: 'users_channels' })
  public members: ManyToMany<typeof User>

  public static schema = {
    type: field('enum', values(ChannelType)),
    members: field('array', field('string')(false, false)),
  }
}

export namespace Channel
{
  export import Type = ChannelType
}

export default Channel
