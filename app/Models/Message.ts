import { DateTime } from 'luxon'
import { BaseModel, belongsTo, BelongsTo, column } from '@ioc:Adonis/Lucid/Orm'
import Channel from 'App/Models/Channel'
import User from 'App/Models/User'
import { rules } from '@ioc:Adonis/Core/Validator'
import { field } from 'App/Utils/Validator'

export default class Message extends BaseModel 
{
  @column({ isPrimary: true })
  public id: number | string

  @column()
  public authorId: number | string

  @column()
  public channelId: number | string

  @column()
  public content: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(() => User, { foreignKey: 'authorId' })
  public author: BelongsTo<typeof User>

  @belongsTo(() => Channel)
  public channel: BelongsTo<typeof Channel>

  public static schema = {
    content: field('string', {
      trim: true
    }, [
      rules.minLength(1),
      rules.maxLength(2048),
    ]),
  }
}
