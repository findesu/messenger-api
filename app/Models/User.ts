import { DateTime } from 'luxon'
import Hash from '@ioc:Adonis/Core/Hash'
import { column, beforeSave, BaseModel, manyToMany, ManyToMany, hasMany, HasMany } from '@ioc:Adonis/Lucid/Orm'
import { rules } from '@ioc:Adonis/Core/Validator'
import { field } from 'App/Utils/Validator'
import Channel from 'App/Models/Channel'
import Message from './Message'

export default class User extends BaseModel 
{
  @column({ isPrimary: true })
  public id: number | string

  @column()
  public avatar?: string

  @column()
  public name: string

  @column()
  public username: string

  @column()
  public email: string

  @column({ serializeAs: null })
  public password: string

  @column({ serializeAs: null })
  public rememberMeToken?: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
  
  @hasMany(() => Message, { foreignKey: 'authorId' })
  public messages: HasMany<typeof Message>

  @manyToMany(() => Channel, { pivotTable: 'users_channels' })
  public channels: ManyToMany<typeof Channel>

  @beforeSave()
  public static async hashPassword(user: User) 
  {
    if (user.$dirty.password) 
      user.password = await Hash.make(user.password)
  }

  public static schema = {
    avatar: field('file', {
      size: '2mb',
      extnames: [ 'gif', 'jpeg', 'jpg', 'png' ],
    }),
    name: field('string', { trim: true }, [
      rules.minLength(1),
      rules.maxLength(255),
    ]),
    username: field('string', { trim: true }, [
      rules.minLength(4),
      rules.maxLength(255),
    ]),
    email: field('string', { trim: true }, [
      rules.maxLength(255),
      rules.email(),
    ]),
    password: field('string', { trim: true }, [
      rules.minLength(8),
      rules.maxLength(255),
    ]),
  }
}
