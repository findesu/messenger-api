function compare(value1: unknown, value2: unknown, strictly?: boolean)
{
  return strictly && value1 === value2 || value1 == value2
}

export function removeDuplicates<T>(array: T[], strictly?: boolean): T[]
{
  return array.filter((value1, index) =>
    array.findIndex(value2 =>
      compare(value1, value2, strictly)
    ) === index
  )
}

export function removeValues<T, U>(array: T[], values: U[], strictly?: boolean): T[]
{
  return array.filter(value1 => values.findIndex(value2 => compare(value1, value2, strictly)) == -1)
}

export default {
  removeDuplicates,
  removeValues,
}
