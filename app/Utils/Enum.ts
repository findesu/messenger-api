import { Enum } from 'App/Utils/Types'

export function keys<T>(object: Enum<T>) : string[]
{
  return Object.keys(object)
}

export function values<T>(object: Enum<T>) : Exclude<T, string>[]
{
  return Object.values(object)
    .filter(value => typeof value !== 'string') as Exclude<T, string>[]
}

export default {
  keys,
  values,
}
