export function isNumeric(value: unknown)
{
  const type = typeof value

  return  type === 'number' ||
          type === 'string' &&
          !isNaN(value as number) &&
          !isNaN(parseFloat(value as string))
}

export function toInteger(value: string)
{
  return Math.floor(Number(value))
}

export default {
  isNumeric,
  toInteger,
}
