import { isNumeric, toInteger } from 'App/Utils/Helpers'
import { clamp } from 'App/Utils/Math'
import { AnyRecord } from 'App/Utils/Types'

interface Pagination
{
  page: number
  amount: number
}

type PaginationDefaults = Partial<Pagination>

export function pagination(qs: AnyRecord, defaults: PaginationDefaults = { page: 1, amount: 50 }): Pagination
{
  return {
    page: isNumeric(qs.page)
      ? Math.max(toInteger(qs.page), 0)
      : defaults.page ?? 1,
      amount: isNumeric(qs.amount)
      ? clamp(toInteger(qs.amount), 0, 50)
      : defaults.amount ?? 50,
  }
}

export default {
  pagination,
}
