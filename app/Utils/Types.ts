export type ValueOf<T> = T[keyof T]
export type UnknownRecord = Record<string, unknown>
/* eslint-disable-next-line @typescript-eslint/no-explicit-any */
export type AnyRecord = Record<string, any>
export type AnyArray = any[]
export type Enum<T> = Record<string, T>
