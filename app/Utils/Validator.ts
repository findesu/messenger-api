import { TypedSchema, SchemaLiteral, SchemaObject, SchemaArray, Rule, schema, AllowedEnumOptions, EnumReturnValue, EnumSetReturnValue } from '@ioc:Adonis/Core/Validator'
import { MultipartFileContract } from '@ioc:Adonis/Core/BodyParser'
import { DateTime } from 'luxon'
import { AnyArray, AnyRecord, UnknownRecord } from './Types'

type SchemaType = 'string' | 'boolean' | 'number' | 'date' | 'enum' | 'enumSet' | 'object' | 'array' | 'file'

type SchemaTree = SchemaLiteral | SchemaObject | SchemaArray

type TypeSchema<T = unknown, U extends SchemaTree = SchemaTree> = {
  t: T
  getTree(): U
}

type CompilerResult<
  O extends boolean,
  N extends boolean,
  T = unknown,
  U extends SchemaTree = SchemaTree,
> = TypeSchema<
  T |
  (O extends true ? undefined : never) |
  (N extends true ? null : never),
  U
>

type AllowedArrayOptions = TypeSchema<unknown, SchemaTree>

type SchemaTypeOptions = {
  string: {
    escape?: boolean
    trim?: boolean
  }

  date: {
    format?: string
  }

  file: {
    size?: string | number
    extnames?: string[]
  }
}

type Compiler<
  T = unknown,
  U extends SchemaTree = SchemaTree,
> =
  <
    O extends boolean = false,
    N extends boolean = false,
  >
  (optional?: O, nullable?: N, rules?: Rule[]) =>
    CompilerResult<O, N, T, U>

type FieldParameters = [
  options: UnknownRecord,
  rules?: Rule[],
] | [
  rules?: Rule[],
]

type ParsedFieldParameters = {
  options?: UnknownRecord,
  rules?: Rule[],
}

function parse(type: SchemaType, ...args: FieldParameters): ParsedFieldParameters
{
  const result: ParsedFieldParameters = { }

  if (type === 'enum' || type === 'enumSet')
  {
    result.options = args[0] as UnknownRecord
    result.rules = args[1]
  }
  else if (Array.isArray(args[0]))
    result.rules = args[0]
  else if (typeof args[0] === 'object')
  {
    result.options = args[0]
    result.rules = args[1]
  }

  return result
}

function compile(type: SchemaType, parsed: ParsedFieldParameters): FieldParameters
{
  if (type === 'array' || type === 'object')
    return [ parsed.rules ?? [ ] ]

  const result: FieldParameters = [ parsed.options ?? parsed.rules ?? [ ] ] as FieldParameters

  if (parsed.options)
    result.push(parsed.rules ?? [ ])

  return result
}

export function field(type: 'string', options: SchemaTypeOptions['string'], rules?: Rule[]): Compiler<string, SchemaLiteral>
export function field(type: 'string', rules?: Rule[]): Compiler<string, SchemaLiteral>
export function field(type: 'boolean', rules?: Rule[]): Compiler<boolean, SchemaLiteral>
export function field(type: 'number', rules?: Rule[]): Compiler<number, SchemaLiteral>
export function field(type: 'date', options: SchemaTypeOptions['date'], rules?: Rule[]): Compiler<DateTime, SchemaLiteral>
export function field(type: 'date', rules?: Rule[]): Compiler<DateTime, SchemaLiteral>
export function field<Options extends AllowedEnumOptions>(type: 'enum', options: Options, rules?: Rule[]): Compiler<EnumReturnValue<Options>, SchemaLiteral>
export function field<Options extends AllowedEnumOptions>(type: 'enumSet', options: Options, rules?: Rule[]): Compiler<EnumSetReturnValue<Options>, SchemaLiteral>
export function field<Options extends TypedSchema>(type: 'object', options: Options, rules?: Rule[]): Compiler<{ [P in keyof Options]: Options[P]['t'] }, SchemaObject>
export function field(type: 'object', rules?: Rule[]): Compiler<AnyRecord, SchemaObject>
export function field<Options extends AllowedArrayOptions>(type: 'array', options: Options, rules?: Rule[]): Compiler<Options['t'][], SchemaArray>
export function field(type: 'array', rules?: Rule[]): Compiler<AnyArray, SchemaArray>
export function field(type: 'file', options: SchemaTypeOptions['file'], rules?: Rule[]): Compiler<MultipartFileContract, SchemaLiteral>
export function field(type: 'file', rules?: Rule[]): Compiler<MultipartFileContract, SchemaLiteral>
export function field(type: SchemaType, ...args: unknown[]): unknown
{
  function compiler<
    O extends boolean = false,
    N extends boolean = false,
  >(optional: O, nullable: N, rules: Rule[] = [ ]) : CompilerResult<O, N>
  {
    const parsedParameters = parse(type, ...args as FieldParameters)

    parsedParameters.rules = [ ...(parsedParameters.rules ?? [ ]), ...rules ]

    const parameters = compile(type, parsedParameters)

    let result = optional
      ? nullable
        ? schema[type].nullableAndOptional.apply(null, parameters)
        : schema[type].optional.apply(null, parameters)
      : nullable
        ? schema[type].nullable.apply(null, parameters)
        : schema[type].apply(null, parameters)
    
    if (type === 'array' || type === 'object')
    {
      if (parsedParameters.options)
        result = result.members(parsedParameters.options)
      else
        result = result.anyMembers()
    }
    
    return result as CompilerResult<O, N>
  }

  return compiler
}

export default {
  field,
}
