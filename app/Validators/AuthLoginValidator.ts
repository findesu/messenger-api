import { schema, rules, CustomMessages } from '@ioc:Adonis/Core/Validator'
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'

export default class AuthLoginValidator 
{
  constructor(protected ctx: HttpContextContract) 
  {}

  public schema = schema.create({
    username: User.schema.username(true, false, [ rules.requiredIfNotExists('email') ]),
    email: User.schema.email(true, false, [ rules.requiredIfNotExists('username') ]),
    password: User.schema.password(false, false),
  })

  public messages: CustomMessages = {

  }
}
