import { schema, CustomMessages } from '@ioc:Adonis/Core/Validator'
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Channel from 'App/Models/Channel'

export default class ChannelCreateValidator 
{
  constructor(protected ctx: HttpContextContract) 
  {}

  public schema = schema.create({
    type: Channel.schema.type(false, false),
    members: Channel.schema.members(false, false),
  })

  public messages: CustomMessages = {}
}
