import { schema, CustomMessages } from '@ioc:Adonis/Core/Validator'
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Channel from 'App/Models/Channel'

export default class ChannelMembersValidator 
{
  constructor(protected ctx: HttpContextContract) 
  {}

  public schema = schema.create({
    ids: Channel.schema.members(false, false),
  })

  public messages: CustomMessages = {}
}
