import { schema, CustomMessages } from '@ioc:Adonis/Core/Validator'
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Message from 'App/Models/Message'

export default class ChannelMessageValidator 
{
  constructor(protected ctx: HttpContextContract) 
  {}

  public schema = schema.create({
    content: Message.schema.content(false, false),
  })

  public messages: CustomMessages = {}
}
