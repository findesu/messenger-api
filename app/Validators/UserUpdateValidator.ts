import { schema, rules, CustomMessages } from '@ioc:Adonis/Core/Validator'
import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'

export default class UserUpdateValidator 
{
  constructor(protected ctx: HttpContextContract) 
  {}

  public schema = schema.create({
    avatar: User.schema.avatar(true, true),
    name: User.schema.name(true, false),
    username: User.schema.username(true, false),
    email: User.schema.email(true, false),
    password: User.schema.password(true, false),
    confirmation: User.schema.password(true, false, [
      rules.requiredIfExistsAny([ 'username', 'email', 'password' ]),
    ]),
  })

  public messages: CustomMessages = {

  }
}
