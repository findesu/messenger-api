import Factory from '@ioc:Adonis/Lucid/Factory'
import Channel from 'App/Models/Channel'
import Message from 'App/Models/Message'
import User from 'App/Models/User'

export const UserFactory = Factory
  .define(User, ({ faker }) => 
  {
    const gender = faker.helpers.arrayElement<'female' | 'male'>([ 'female', 'male' ])
    const firstName = faker.name.firstName(gender)
    const lastName = faker.name.lastName(gender)

    return {
      name: `${firstName} ${lastName}`,
      username: faker.internet.userName(firstName, lastName),
      email: faker.internet.email(firstName, lastName),
      password: faker.internet.password(),
    }
  })
  .relation('channels', () => ChannelFactory)
  .relation('messages', () => MessageFactory)
  .build()

export const ChannelFactory = Factory
  .define(Channel, () => ({ }))
  .state('dm', channel => channel.type = Channel.Type.DM)
  .state('group', channel => channel.type = Channel.Type.GROUP)
  .relation('messages', () => MessageFactory)
  .relation('members', () => UserFactory)
  .build()

export const MessageFactory = Factory
  .define(Message, ({ faker }) => ({
    content: faker.lorem.sentences(faker.datatype.number({
      min: 1,
      max: 3,
    })),
  }))
  .relation('author', () => UserFactory)
  .relation('channel', () => ChannelFactory)
  .build()
