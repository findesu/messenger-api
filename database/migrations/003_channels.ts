import BaseSchema from '@ioc:Adonis/Lucid/Schema'
import Channel from 'App/Models/Channel'

export default class extends BaseSchema 
{
  protected tableName = 'channels'

  public async up() 
  {
    this.schema.createTable(this.tableName, table => 
    {
      table.bigIncrements('id')
      table.enum('type', Object.values(Channel.Type))
      table.timestamp('created_at', { useTz: true }).notNullable()
    })
  }

  public async down() 
  {
    this.schema.dropTable(this.tableName)
  }
}
