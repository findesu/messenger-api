import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema 
{
  protected tableName = 'users_channels'

  public async up() 
  {
    this.schema.createTable(this.tableName, table => 
    {
      table.bigInteger('user_id').notNullable().unsigned().references('id').inTable('users').onDelete('cascade')
      table.bigInteger('channel_id').notNullable().unsigned().references('id').inTable('channels').onDelete('cascade')

      table.unique([ 'user_id', 'channel_id' ])
    })
  }

  public async down() 
  {
    this.schema.dropTable(this.tableName)
  }
}
