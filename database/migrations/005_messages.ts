import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema 
{
  protected tableName = 'messages'

  public async up() 
  {
    this.schema.createTable(this.tableName, table => 
    {
      table.bigIncrements('id')
      table.bigInteger('author_id').nullable().unsigned().references('id').inTable('users').onDelete('set null')
      table.bigInteger('channel_id').notNullable().unsigned().references('id').inTable('channels').onDelete('cascade')
      table.string('content', 2048).notNullable()
      table.timestamp('created_at', { useTz: true }).notNullable()
      table.timestamp('updated_at', { useTz: true }).notNullable()
    })
  }

  public async down() 
  {
    this.schema.dropTable(this.tableName)
  }
}
