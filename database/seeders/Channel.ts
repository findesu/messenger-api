import faker from '@faker-js/faker'
import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import User from 'App/Models/User'
import { ChannelFactory } from 'Database/factories'

export default class extends BaseSeeder 
{
  public async run() 
  {
    const user = await User.findBy('username', 'fin_')

    if (!user)
      return
    
    const users = faker.helpers.arrayElements(
      (await User.query().whereNot('id', user.id)),
      10,
    )
    
    if (users.length == 0)
      return

    await ChannelFactory
      .with('members', 1, instance => instance.merge(user))
      .with('members', 1, instance => instance.merge(users.shift()!))
      .apply('dm')
      .createMany(users.length)
  }
}
