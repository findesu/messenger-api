import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Application from '@ioc:Adonis/Core/Application'
import Logger from '@ioc:Adonis/Core/Logger'

export default class extends BaseSeeder 
{
  private async runSeeder(name: string)
  {
    const seeder: { default: typeof BaseSeeder } = await import(`Database/seeders/${name}`)

    if (seeder.default.developmentOnly && !Application.inDev)
      return

    await new seeder.default(this.client).run()

    if (Logger.isLevelEnabled('info'))
      Logger.info('Completed %s seeder', name)
  }
  
  public async run()
  {
    await this.runSeeder('User')
    await this.runSeeder('Channel')
    await this.runSeeder('Message')
  }
}
