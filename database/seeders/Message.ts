import faker from '@faker-js/faker'
import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Channel from 'App/Models/Channel'
import { MessageFactory } from 'Database/factories'

export default class extends BaseSeeder 
{
  public async run() 
  {
    const channels = await Channel.query().preload('members')

    for (const channel of channels)
    {
      await MessageFactory
        .merge({ channelId: channel.id })
        .with('author', 1, instance => instance.merge(faker.helpers.arrayElement(channel.members)))
        .createMany(faker.datatype.number({
          min: 1,
          max: 50,
        }))
    }
  }
}
