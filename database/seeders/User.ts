import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import { UserFactory } from 'Database/factories'

export default class extends BaseSeeder 
{
  public async run() 
  {
    await UserFactory
      .merge([
        {
          name: 'Fin',
          username: 'fin_',
          email: 'fin@gmail.com',
          password: 'fin12345',
        },
      ])
      .createMany(50)
  }
}
