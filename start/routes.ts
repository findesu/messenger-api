import Route from '@ioc:Adonis/Core/Route'

/*
|--------------------------------------------------------------------------
| Authorization
|--------------------------------------------------------------------------
|
*/
Route.group(() => 
{
  Route.post('login', 'AuthController.login').as('login')

  Route.group(() => 
  {
    Route.post('logout', 'AuthController.logout').as('logout')
    Route.post('refresh', 'AuthController.refresh').as('refresh')
  })
    .middleware('auth')
})
  .as('auth')

/*
|--------------------------------------------------------------------------
| Other
|--------------------------------------------------------------------------
|
*/
Route.group(() => 
{
  Route.resource('users', 'UsersController').apiOnly()

  Route.resource('channels', 'ChannelsController').only([ 'index', 'store', 'show', 'destroy' ])

  Route.resource('channels.members', 'ChannelMembersController').only([ 'index', 'store' ])
  Route.delete('/channels/:channel_id/members', 'ChannelMembersController.destroy').as('channels.members.destroy')

  Route.resource('channels.messages', 'ChannelMessagesController').apiOnly()
})
  .middleware('auth')
